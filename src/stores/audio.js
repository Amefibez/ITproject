
import { defineStore } from 'pinia'
export const useAudioStore = defineStore('audio', {
    state() {
      return {
       _token: "646af386d32f0312d1fdd36b78d323d07a1995e1",
       audioUrl: '',
      }
    },
    getters:{
        token(state){
          if (!state._token){
            state._token = localStorage.getItem('token', this._token)
          }
          return state._token
        }
      },
      actions:{
          async sendText(text){
          let {isSuccess,result} = await this.makeRequest({
            method:'post',
            url: '/tts/',
            data: {
              text,
              format: "wav"
            },
          })
          if(isSuccess)
          this.audioUrl = result.result_url
          return{isSuccess,result}
        }
      },
})