import { createApp } from 'vue'
import { createPinia } from 'pinia'
import './style.css'
import App from './App.vue'
import ElementPlus from 'element-plus'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import 'element-plus/dist/index.css'
import { axiosPiniaPlugin } from './stores/axiosPlugin'




const pinia = createPinia()
pinia.use(axiosPiniaPlugin)
const app =createApp(App)

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

app.use(ElementPlus)
    .use(pinia)
    .mount('#app')
